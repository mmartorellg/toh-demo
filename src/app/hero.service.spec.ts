import { TestBed, inject } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

import { HeroService } from './hero.service';
import { environment } from 'src/environments/environment';

describe('HeroService', () => {
  let service: HeroService;
  let httpMock: HttpTestingController;
  let result = { id: 11, name: 'Dr Nice' };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HeroService]
    });
    service = TestBed.inject(HeroService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', inject([HeroService], (service: HeroService) => {
    expect(service).toBeTruthy();
  }));

  it('getHero', () => {
    service.getHero(1).subscribe((res) => {
      expect(res).toEqual(result);
    });
    const req = httpMock.expectOne('api/heroes/1');
    expect(req.request.method).toBe('GET');
    req.flush(result);
  });
});